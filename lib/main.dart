import 'package:expenseman/models/User.dart';
import 'package:expenseman/services/AuthService.dart';
import 'package:expenseman/ui/screens/UserProfilePage.dart';
import 'package:expenseman/ui/screens/Wrapper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Expense Manager',
        routes: {
          '/': (context) => Wrapper(),
          '/user-profile': (context) => UserProfilePage(),
        },
        initialRoute: '/',
      ),
    );
  }
}
