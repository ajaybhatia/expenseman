import 'package:expenseman/models/User.dart';
import 'package:expenseman/services/DatabaseService.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future signInWithEmailAndPassword(
      {@required String email, @required String password}) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;

      return _userFromFirebaseUser(user);
    } catch (error) {
      throw Exception(error.message);
    }
  }

  Future registerWithEmailAndPassword(
      {@required String firstName,
      @required String lastName,
      @required String email,
      @required String password}) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;

      await DatabaseService(uid: user.uid)
          .updateUserProfile(firstName, lastName);
      return _userFromFirebaseUser(user);
    } catch (error) {
      throw Exception(error.message);
    }
  }

  void signOut() async {
    await _auth.signOut();
  }
}
