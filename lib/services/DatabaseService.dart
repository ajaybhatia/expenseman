import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:expenseman/models/UserProfile.dart';

class DatabaseService {
  final String uid;

  DatabaseService({this.uid});

  final CollectionReference usersCollection =
      Firestore.instance.collection('users');

  Future<void> updateUserProfile(String firstName, String lastName) async {
    return await usersCollection
        .document(uid)
        .setData({'firstName': firstName, 'lastName': lastName});
  }

  UserProfile _userProfileFromSnapshot(DocumentSnapshot snapshot) {
    return UserProfile(
        firstName: snapshot.data['firstName'],
        lastName: snapshot.data['lastName']);
  }

  Stream<UserProfile> get userProfile {
    return usersCollection
        .document(uid)
        .snapshots()
        .map(_userProfileFromSnapshot);
  }
}
