class UserProfile {
  final String firstName;
  final String lastName;

  UserProfile({this.firstName, this.lastName});
}
