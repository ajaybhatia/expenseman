import 'package:expenseman/models/User.dart';
import 'package:expenseman/ui/screens/authentication/AuthPage.dart';
import 'package:expenseman/ui/screens/home/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null) {
      return AuthPage();
    } else {
      return HomePage();
    }
  }
}
