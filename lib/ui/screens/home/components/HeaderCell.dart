import 'package:flutter/material.dart';

class HeaderCell extends StatelessWidget {
  final String title;
  final String amount;
  final Color color;
  final bool isLast;

  HeaderCell(
      {@required this.title,
      @required this.amount,
      this.color = Colors.black,
      this.isLast = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: !isLast ? EdgeInsets.only(right: 30) : EdgeInsets.zero,
      decoration: !isLast
          ? BoxDecoration(
              border: Border(
                right: BorderSide(color: Colors.black),
              ),
            )
          : null,
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
          SizedBox(height: 10),
          Text(
            '\u20B9$amount',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w900,
              color: color,
            ),
          ),
        ],
      ),
    );
  }
}
