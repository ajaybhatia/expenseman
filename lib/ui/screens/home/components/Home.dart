import 'package:expenseman/models/UserProfile.dart';
import 'package:expenseman/services/DatabaseService.dart';
import 'package:expenseman/ui/screens/home/components/Header.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  final String uid;

  Home({this.uid});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        StreamBuilder<UserProfile>(
          stream: DatabaseService(uid: uid).userProfile,
          builder: (context, snapshot) {
            UserProfile userProfile = snapshot.data;
            if (snapshot.hasData) {
              return Text(
                'Welcome back, ${userProfile.firstName}!',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              );
            }
            return Container();
          },
        ),
        SizedBox(height: 10),
        Header(),
      ],
    );
  }
}
