import 'package:expenseman/models/User.dart';
import 'package:expenseman/ui/screens/authentication/AuthPage.dart';
import 'package:expenseman/ui/screens/home/components/Home.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null) {
      return AuthPage();
    } else {
      return Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: -20,
              child: Container(
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  color: Colors.blue[900],
                ),
              ),
            ),
            Positioned(
              top: -80,
              left: 0,
              child: Container(
                height: 120,
                width: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(120),
                  color: Colors.blue[900],
                ),
              ),
            ),
            Positioned(
              top: 90,
              left: 20,
              right: 20,
              bottom: 20,
              child: Container(
                child: Home(uid: user.uid),
              ),
            ),
            Positioned(
              child: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                actions: <Widget>[
                  IconButton(
                    icon: Icon(
                      FontAwesomeIcons.userCircle,
                      color: Colors.black,
                    ),
                    onPressed: () =>
                        Navigator.pushNamed(context, '/user-profile'),
                  ),
                ],
              ),
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Text('Drawer Header'),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue[900],
          foregroundColor: Colors.white,
          elevation: 10,
          child: Icon(FontAwesomeIcons.plus),
          onPressed: () {
            showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (BuildContext context) {
                return Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 32,
                  ),
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topRight,
                        child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () => Navigator.of(context).pop(),
                        ),
                      )
                    ],
                  ),
                );
              },
            );
          },
        ),
      );
    }
  }
}
