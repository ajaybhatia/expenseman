import 'package:expenseman/models/User.dart';
import 'package:expenseman/models/UserProfile.dart';
import 'package:expenseman/services/AuthService.dart';
import 'package:expenseman/services/DatabaseService.dart';
import 'package:expenseman/ui/screens/authentication/AuthPage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class UserProfilePage extends StatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  final AuthService _authService = AuthService();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();

  bool _edit = false;

  void _signOut() {
    _authService.signOut();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);

    if (user == null) {
      return AuthPage();
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: StreamBuilder<UserProfile>(
          stream: DatabaseService(uid: user.uid).userProfile,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              UserProfile userProfile = snapshot.data;
              String abbrevation =
                  '${userProfile.firstName[0].toUpperCase()}${userProfile.lastName[0].toUpperCase()}';

              _firstNameController.text = userProfile.firstName;
              _lastNameController.text = userProfile.lastName;

              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: CircleAvatar(
                          radius: 48,
                          backgroundColor: Colors.blue[900],
                          foregroundColor: Colors.white,
                          child: Text(
                            abbrevation,
                            style: TextStyle(
                              fontSize: 36,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: IconButton(
                          icon: _edit
                              ? Icon(
                                  FontAwesomeIcons.times,
                                  size: 14,
                                )
                              : Icon(
                                  FontAwesomeIcons.pencilAlt,
                                  size: 14,
                                ),
                          onPressed: () => setState(() => _edit = !_edit),
                        ),
                      ),
                      SizedBox(height: 50),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Column(
                          children: <Widget>[
                            TextField(
                              decoration: InputDecoration(
                                hintText: 'First Name',
                                contentPadding: EdgeInsets.symmetric(
                                  vertical: 16,
                                  horizontal: 8,
                                ),
                                filled: !_edit,
                                fillColor: Colors.grey[200],
                                border: OutlineInputBorder(),
                              ),
                              // initialValue: userProfile.firstName,
                              readOnly: !_edit,
                              controller: _firstNameController,
                            ),
                            SizedBox(height: 10),
                            TextField(
                              decoration: InputDecoration(
                                hintText: 'Last Name',
                                contentPadding: EdgeInsets.symmetric(
                                  vertical: 16,
                                  horizontal: 8,
                                ),
                                filled: !_edit,
                                fillColor: Colors.grey[200],
                                border: OutlineInputBorder(),
                              ),
                              // initialValue: userProfile.lastName,
                              readOnly: !_edit,
                              controller: _lastNameController,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      MaterialButton(
                        minWidth: MediaQuery.of(context).size.width * 0.7,
                        onPressed: _edit
                            ? () async {
                                await DatabaseService(uid: user.uid)
                                    .updateUserProfile(
                                        _firstNameController.text,
                                        _lastNameController.text);
                                setState(() {
                                  _edit = !_edit;
                                });
                              }
                            : null,
                        color: Colors.blue[900],
                        textColor: Colors.white,
                        shape: StadiumBorder(),
                        padding: EdgeInsets.symmetric(
                          horizontal: 100,
                          vertical: 15,
                        ),
                        child: _edit ? Text('Updatte') : null,
                      ),
                      SizedBox(height: 50),
                      FlatButton(
                        onPressed: _signOut,
                        child: Text(
                          'Sign Out',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      );
    }
  }
}
