import 'package:expenseman/services/AuthService.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  final Function toggleView;

  RegisterPage({this.toggleView});

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final AuthService _authService = AuthService();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String _firstName;
  String _lastName;
  String _email;
  String _password;
  bool _loading = false;

  @override
  void initState() {
    super.initState();
  }

  void _register() async {
    if (_formKey.currentState.validate()) {
      setState(() => _loading = true);
      try {
        await _authService.registerWithEmailAndPassword(
            firstName: _firstName.trim(),
            lastName: _lastName.trim(),
            email: _email.trim(),
            password: _password.trim());
      } catch (error) {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(error.message),
            action: SnackBarAction(
              label: 'DISMISS',
              onPressed: () => _scaffoldKey.currentState.hideCurrentSnackBar(),
            ),
          ),
        );
      }
      setState(() => _loading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlutterLogo(
                size: 256,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('First Name'),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onChanged: (value) => setState(() => _firstName = value),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'First name cannot be empty';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Last Name'),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onChanged: (value) => setState(() => _lastName = value),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Last name cannot be empty';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Email'),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      enableSuggestions: true,
                      keyboardType: TextInputType.emailAddress,
                      textCapitalization: TextCapitalization.none,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onChanged: (value) => setState(() => _email = value),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Email cannot be empty';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Password'),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 10,
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                      ),
                      obscureText: true,
                      onChanged: (value) => setState(() => _password = value),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Password cannot be empty';
                        }
                        return null;
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 25,
              ),
              MaterialButton(
                minWidth: MediaQuery.of(context).size.width * 0.7,
                onPressed: _loading ? null : _register,
                color: Colors.blue[900],
                textColor: Colors.white,
                shape: StadiumBorder(),
                padding: EdgeInsets.symmetric(
                  horizontal: 100,
                  vertical: 15,
                ),
                child: _loading
                    ? SizedBox(
                        height: 14,
                        width: 14,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                    : Text('Sign Up'),
                disabledColor: Colors.blue,
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Already have a Account?'),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                    onTap: () => widget.toggleView(),
                    child: Text(
                      'Sign In',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
